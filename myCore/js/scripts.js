(function(window, $){
    function scrollTo(target) {
        var wheight = $(window).height();
        
        var ooo = $(target).offset().top;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    	$(document).ready(function() {
        $(window).resize(function() {                
            if ($("#top-menu-resp").is(":visible")){
                $("#header-top ul").hide();
            }
            else{                    
                $("#header-top ul").show();
            }
        });

        $("#top-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#header-top ul").toggle("slow");
        });
        
        $("#slider_home").lightSlider({
            item: 1,
            pager: true,
            enableDrag: true,
            controls: true,
            loop: true
        });
        
        var $slider_toggle = $("#slider-toggle").lightSlider({
            item: 3,
            pager: false,
            enableDrag: true,
            controls: true,
            loop: true
        });
        
        $(".link-toggle").click(function(e) {
            e.preventDefault();
            $("#toggle").toggle("slow", function() {
                if ($("#toggle").is(":visible")){
                    $slider_toggle.refresh();
                    $(".link-toggle.link-img").addClass("flecha-abajo").removeClass("flecha-arriba");
                    $(".link-toggle.link-texto").html("-Spencial");
                }
                else{
                    $(".link-toggle.link-img").addClass("flecha-arriba").removeClass("flecha-abajo");
                    $(".link-toggle.link-texto").html("+Spencial");
                }
            });
            
        });

        $(document).foundation();
	});
})(window, jQuery);