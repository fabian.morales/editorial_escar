create table arc_my_cart_carrito(
	id integer not null auto_increment primary key,
	id_user integer default 0 not null,
	id_sesion varchar(255) default ' ' not null,
	id_referencia integer,	
	id_ext integer DEFAULT NULL,
	cantidad integer default 0 not null,
	fecha timestamp default current_timestamp,
	foreign key (id_referencia) references arc_my_cat_referencia (id) on delete cascade,
	foreign key (id_ext) references arc_my_cat_extension (id) on delete cascade,
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table arc_my_cart_pedido(
	id integer not null auto_increment,
	id_user integer not null,
	direccion varchar(512),
	num_items integer not null,
	cargo_envio numeric(10,3) default 0 not null,
	valor_items numeric(10,3) default 0 not null,
	valor_total numeric(10,3) default 0 not null,
	porc_iva numeric(10,3) default 0 not null,
	valor_iva numeric(10,3) default 0 not null,
	fecha timestamp,
	estado enum('N', 'R', 'A', 'P', 'E') default 'N',
	cod_trans varchar(255),
	primary key (id),
	foreign key (id_user) references arc_my_sis_user (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;


CREATE TABLE arc_my_cart_pedidodet (
	id integer not null auto_increment,
	id_pedido INT(11) NOT NULL DEFAULT '0',
	id_referencia INT(11) NOT NULL,	
	id_ext INT(11),
	cantidad INT(11) NOT NULL DEFAULT '0',
	valor DECIMAL(10,3) NOT NULL DEFAULT '0.000',
	porc_iva DECIMAL(10,3) NOT NULL DEFAULT '0.000',
	valor_iva DECIMAL(10,3) NOT NULL DEFAULT '0.000',
	PRIMARY KEY (id),
	FOREIGN KEY (id_pedido) REFERENCES arc_my_cart_pedido (id),
	FOREIGN KEY (id_referencia) REFERENCES arc_my_cat_referencia (id),	
	FOREIGN KEY (id_ext) REFERENCES arc_my_cat_extension (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE=InnoDB;