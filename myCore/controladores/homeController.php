<?php

class homeController extends myController{
    public function index(){
        return myView::render("home.index");
    }
    
    public function educacion(){
        return myView::render("home.linea_educativa");
    }
    
    public function salud(){
        return myView::render("home.linea_salud");
    }
    
    public function variedad(){
        return myView::render("home.linea_variedad");
    }

    public function cocina(){
        return myView::render("home.linea_cocina");
    }
    
    public function literatura(){
        return myView::render("home.linea_literatura");
    }
    
    public function saludar($name, $apellido){
        return "Hola ".$name." ".$apellido." :)";
    }
    
    public function verificarCodigo($cod){
        $codigo = CodigoApp::where("codigo", $cod)->first();
        
        $ret = array();
        if (sizeof($codigo)){
            if ($codigo->estado == "V"){
                $ret["error"] = 0;
                $ret["valor"] = "OK";
                $ret["cd"] = $cod;
            }
            else{
                $ret["error"] = 1;
                $ret["valor"] = "Codigo no valido";
            }
        }
        else{
            $ret["error"] = 1;
            $ret["valor"] = "Codigo inexistente";
        }
        
        return "retorno(".json_encode($ret).")";
    }
    
    public function registrarTrial($idDisp){
        $trial = TrialApp::where("id_dispositivo", $idDisp)->get();
        if (sizeof($trial)){
            $ret = array();
            $ret["success"] = false;
            $ret["message"] = 'El dispositivo ya se encuentra registrado';
            
            return "retorno(".json_encode($ret).")";
        }
        
        date_default_timezone_set('America/Bogota');
        $trial = new TrialApp();
        $trial->id_dispositivo = $idDisp;
        $trial->fecha = date('Y-m-d: H:i:s');
        $trial->estado = 'V';
        
        if ($trial->save()){
            $ret = array();
            $ret["success"] = true;
            
            return "retorno(".json_encode($ret).")";
        }
        else{
            $ret = array();
            $ret["success"] = false;
            $ret["message"] = 'No se pudo realizar el registro';
            
            return "retorno(".json_encode($ret).")";
        }
    }
    
    public function validarTrial($idDisp){
        $trial = TrialApp::where("id_dispositivo", $idDisp)->first();
        if (!sizeof($trial)){
            $ret = array();
            $ret["success"] = false;
            $ret["message"] = 'El dispositivo no se encuentra registrado';
            
            return "retorno(".json_encode($ret).")";
        }
        //print_r($trial); die();
        date_default_timezone_set('America/Bogota');
        if ($trial->estado != 'V'){
            $ret = array();
            $ret["success"] = false;
            $ret["message"] = 'El dispositivo no es válido';
            
            return "retorno(".json_encode($ret).")";
        }
        
        date_default_timezone_set('America/Bogota');
        
        $date1 = new DateTime($trial->fecha);
        $date2 = new DateTime(date());

        $dias = $date2->diff($date1)->format("%a");        
        
        if ($dias > 5){
            $ret = array();
            $ret["success"] = false;
            $ret["message"] = 'El periodo de prueba se venció';
            
            return "retorno(".json_encode($ret).")";
        }
        
        $ret = array();
        $ret["success"] = true;

        return "retorno(".json_encode($ret).")";
    }
}