var mainController = function($, window, _){
    window.utils.establecerFondo("pantalla_inicio");
    
    var $codigo = localStorage.getItem("escar.codigo");
    
    window.ambiente = window.utils.reproducirSonido("ambiente", true);
    
    if (!_.isNull($codigo) && !_.isEmpty($codigo)){
        $("#inicio").load('./validador.html', function(res) {
            $("#lnkEmpezar").css('display', 'block');
            $("#lnkInstrucciones").css('display', 'block');
            $("#validacion").css('display', 'none');
            $("#btnValidar").css('display', 'none');

            $("#lnkEmpezar").click(function(e) {
                e.preventDefault();
                selectorOpController($, window, _);
            });

            $("#lnkInstrucciones").click(function(e) {
                e.preventDefault();
                ayudaController($, window, _);
            });
        });
    }
    else{
        $("#lnkEmpezar").css('display', 'none');
        $("#lnkInstrucciones").css('display', 'none');
        $("#validacion").css('display', 'block');        
        
        $("#inicio").load('./validador.html', function(res) {
            validadorController($, window, _);
        });
    }
};