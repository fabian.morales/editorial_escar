var utils = function(){
    return {
        establecerFondo: function($fondo) {
            $("body").removeClass().addClass($fondo);    
        },
        
        precargarSonidos: function(){
            var $sonidos = ["ambiente", "fin_tiempo", "inicio_op", "mala_posicion", "victoria"];
            $.each($sonidos, function(i, o){
                $.ajax({
                    url: './' + o + '.mp3'
                });
            });
        },
        
        reproducirSonido: function($sonido, $loop){
            var $audio = new Audio();
            //var $audio = new Audio('./sonidos/'+$sonido+'.mp3');
            if ($audio.canPlayType('audio/ogg; codecs="vorbis"')){
                $audio.src = ('./sonidos/'+$sonido+'.ogg');
            }
            else{
                $audio.src = ('./sonidos/'+$sonido+'.mp3');
            }

            $audio.load();

            if ($loop === true){
                $audio.addEventListener('ended', function() {
                    this.currentTime = 0;
                    this.play();
                }, false);
            }
            
            $audio.play();
        }
    };
};