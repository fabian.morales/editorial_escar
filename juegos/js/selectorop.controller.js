var selectorOpController = function($, window, _){
        
    $("#inicio").load('./selector_op.html', function(res) {
        window.utils.establecerFondo('fondo2');
        $("#lnkInstrucciones").click(function(e) {
            e.preventDefault();
            ayudaController($, window, _);
        });
    
        $("a[rel='lnkOp']").click(function(e) {
            e.preventDefault();                            
            var $op = $(this).attr("data-op");
            $("#inicio").load('./tablero.html', function(res) {
                tableroController($, window, _, $op);
            });
        });
    });
};