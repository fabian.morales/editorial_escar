var tableroController = function($, window, _, $op){
    
    var $numFichas = 8;
    var $numFilas = 8;
    var $numColumnas = 8;
    var $fichas = [];
    var $operaciones = [];
    var $valores = [];
    var $celdas = [];
    var $clasesNum = ['cero', 'uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve'];
    var $historial = [];
    
    var $claseFondo = "fondo" + (Math.floor(Math.random() * 4) + 1);
    window.utils.establecerFondo($claseFondo);
    
    var $titulos = [];
    $titulos["+"] = "Suma";
    $titulos["-"] = "Resta";
    $titulos["/"] = "Divisi&oacute;n";
    $titulos["*"] = "Multiplicaci&oacute;n";
    $titulos["frac"] = "Fraccionarios";
    
    var $clases = ['fucsia', 'verde', 'cyan', 'fucsia', 'verde', 'cyan'];
    var $clase = Math.floor(Math.random() * 5);
    $("#titulo_tablero").addClass($clases[$clase]);
    $("#titulo_tablero").html($titulos[$op]);
    window.utils.reproducirSonido("inicio_op");

    function celda(){
        return {
            ficha: null,
            extremo: null
        };
    }

    function operacion(){
        return {
            operando1: 0.00,
            operando2: 0.00,
            operador: '',
            resultado: 0.00,
            plaza1: 1,
            plaza2: 1,
            toString: function($etq) {
                $etq = (typeof $etq !== 'undefined') || $etq;
                
                var $_op = [];
                $_op["+"] = "+";
                $_op["-"] = "-";
                $_op["/"] = "&divide;";
                $_op["*"] = "&times;";
                $_op["frac"] = "/";
                                
                var $clase1 = "numero " + $clasesNum[Math.floor(Math.random() * 9)];
                var $clase2 = "numero " + $clasesNum[Math.floor(Math.random() * 9)];
                var $clase3 = "numero " + $clasesNum[Math.floor(Math.random() * 9)];
                
                var $ret = $etq === true ? 
                    "<span class='" + $clase1 + "'>" + this.operando1 + "</span><span class='" + $clase2+"'>" + $_op[this.operador] + "</span><span class='" + $clase3 +"'>" + this.operando2 + "</span>" : 
                    this.operando1 + this.operador + this.operando2;
                
                return $ret;
            }
        };
    }

    function extremo(){
        return {
            operacion: '',
            respuesta: 0.00,
            op: null,
            asignarOperacion: function($i, $j) {
                this.op = $valores[$i][$j];
                this.operacion = $valores[$i][$j].toString();							
                this.respuesta = eval($valores[$i][$j].toString().replace("frac", "/"));
            }
        };
    }

    function ficha(){
        return {
            id: 0,
            extremo1: null,
            extremo2: null,
            posicionada: false,
            orientacion: 'v',
            posicion: 1,
            posx: 0,
            posy: 0,
            gridx: 0,
            gridy: 0,
            girar: function($sentido){

                var $clases = ['posicion1', 'posicion2', 'posicion3', 'posicion4'];
                var $div = $("div#" + this.id);
                $div.removeClass($clases[this.posicion - 1]);

                if (this.orientacion === 'v'){
                    this.orientacion = 'h';
                    $div.addClass("horizontal").removeClass("vertical");
                }
                else{					
                    this.orientacion = 'v';
                    $div.addClass("vertical").removeClass("horizontal");
                }

                if ($sentido === 1){
                    if (this.posicion < 4){
                        this.posicion += 1;
                    }
                    else{
                        this.posicion = 1;
                    }                    
                }
                else{
                    if (this.posicion > 1){
                        this.posicion -= 1;
                    }
                    else{
                        this.posicion = 4;
                    }
                }

                $div.addClass($clases[this.posicion - 1]);
            }
        };
    }

    function crearCeldas() {
        for ($i=0; $i<$numFilas; $i++){
            var $fila = [];

            for ($j=0; $j<$numColumnas; $j++){
                $fila.push(celda());
            }

            $celdas.push($fila);
        }
    }

    function crearFichas(){
        for ($i=0; $i<$numFichas; $i++){
            var $ficha = ficha();

            $ficha.id = guid();

            var $extremo1 = extremo();
            var $extremo2 = extremo();

            var $v = Math.floor(Math.random() * $valores.length);
            $extremo1.asignarOperacion($v, 0);
            $extremo2.asignarOperacion($v, 1);
            $valores.splice($v,1);

            $ficha.extremo1 = $extremo1;
            $ficha.extremo2 = $extremo2;
            $ficha.orientacion = 'v';

            $fichas.push($ficha);
            
            var $resp = $ficha.extremo2.respuesta;
            //console.log($ficha.extremo2);
            if ($ficha.extremo2.op.operador === "frac"){                
                $resp = "<img src='img/fracciones/" + $ficha.extremo2.op.operando1 + "_" + $ficha.extremo2.op.operando2 + ".png' class='frac' />";
            }
            
            var $claseFicha =  Math.floor(Math.random() * 4) + 1;
            
            var $clase1 = "numero " + $clasesNum[Math.floor(Math.random() * 9)];

            $div = $("<div class='cajon' id='cajon_" + $ficha.id + "'></div>")
                    .append("<div id='" + $ficha.id + "' class='bloque ficha" + $claseFicha + " vertical libre posicion1' data-ficha='" + $i + "'><div>" + $ficha.extremo1.op.toString(true) + "</div><div><span class='" + $clase1 + "'>" + $resp + "</span></div></div>")
                    .data("ficha", $ficha);

            $("#fichas").append($div);
        }
    }

    function crearOperaciones($tipo){
        for($i=0; $i<$numFichas; $i++){
            do{
                var $op = operacion();
                if($tipo === "frac"){
                    $op.operando1 = Math.floor(Math.random() * 9) + 1;
                    $op.operando2 = Math.floor(Math.random() * 9) + 1;					
                }
                else{
                    $op.operando1 = Math.floor(Math.random() * 12) + 1;
                    $op.operando2 = Math.floor(Math.random() * 9) + 1;	
                }
                
                $op.operador = $tipo;
                var $res = 0;

                if ($tipo === "-"){
                    if ($op.operando2 > $op.operando1){
                        var $temp = $op.operando1;
                        $op.operando1 = $op.operando2;
                        $op.operando2 = $temp;
                    }
                }

                if ($tipo === "/"){
                    $res = $op.operando1 * $op.operando2;
                    var $temp = $op.operando1;
                    $op.operando1 = $res;
                    $res = $temp;
                }
                else if($tipo === "frac"){
                    if ($op.operando1 > $op.operando2){
                        var $temp = $op.operando1;
                        $op.operando1 = $op.operando2;
                        $op.operando2 = $temp;
                    }

                    $res = eval($op.operando1 / $op.operando2);
                }
                else{
                    $res = eval($op.toString());
                }                    

                $op.operador = $tipo;
                $op.resultado = $res;

                var $repetido = false;
                
                $.each($operaciones, function(i, o) {
                    $o = o.toString().replace("frac", "/");
                    
                    if ($op.resultado === eval($o)){
                        $repetido = true;
                        return false;
                    }
                });
            }while($repetido === true);			

            $operaciones.push($op);
        }

        for($i=0; $i<$numFichas; $i++){
            var $sig = $i < ($numFichas - 1) ? $i + 1 : 0;
            $valores.push([$operaciones[$i], $operaciones[$sig]]);
        }

        window.operaciones = $operaciones;
    }

    function guid() {
      function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
              .toString(16)
              .substring(1);
            }

            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    $(document).ready(function() {
        
    });
    
    crearCeldas();
    crearOperaciones($op);		
    crearFichas();

    window.celdas = $celdas;
    
    $( ".bloque" ).draggable({
        "grid": [ 64, 64 ],
        start: function(event, ui) {
            ui.helper.data('dropped', false);
        },
        stop: function(event, ui)
        {
            if (ui.helper.data('dropped') !== true){
                $(ui)[0].helper.css("top", 0);
                $(ui)[0].helper.css("left", 0);
            }
            // Check value of ui.helper.data('dropped') and handle accordingly...
        }
    });
    
    $(".bloque").click(function(e) {
        $(".bloque.actual").each(function(i, o) {
            $(o).removeClass("actual");
        });
        
        $(this).addClass("actual");
    });
    
    $("#giro_izq").click(function(e) {
        var $bloque = $(".bloque.actual");
        
        if ($bloque.length > 0){
            
            if ($bloque.hasClass("libre")){
                var $data = $fichas[$bloque.data('ficha')];
                $data.girar(-1);
            }
        }
    });
    
    $("#giro_der").click(function(e) {
        var $bloque = $(".bloque.actual");
        
        if ($bloque.length > 0){
            
            if ($bloque.hasClass("libre")){
                var $data = $fichas[$bloque.data('ficha')];
                $data.girar(1);
            }
        }
    });

    $("#tablero").droppable({
        drop: function(event, ui){
            ui.draggable.data('dropped', true);
            var $ficha = $fichas[$(ui)[0].helper.data("ficha")];
            var $posFicha = $(ui)[0].helper.position();
            var $offset = $("#tablero").offset();
            
            //console.log("posficha: ");
            //console.log($posFicha);
            
            //console.log("offset: ");
            //console.log($offset);
            
            //console.log("abs: ");
            //console.log(ui.draggable.position());

            $pos1 = { x: Math.round(($posFicha.left - $offset.left) / 64), y: Math.round(($posFicha.top - $offset.top) / 64) };
            $pos2 = {};

            if ($ficha.orientacion === 'v'){
                $pos2 = { x: $pos1.x, y: $pos1.y + 1 };
            }
            else{
                $pos2 = { x: $pos1.x + 1, y: $pos1.y };
            }
            
            //console.log($pos1);
            //console.log($pos2);

            if (_.isEmpty($celdas[$pos1.x][$pos1.y].ficha) && _.isEmpty($celdas[$pos2.x][$pos2.y].ficha)){
                var $posicionar = false;
                if ($(".bloque.fijo").size() === 0){
                    $posicionar = true;
                }
                else{
                    var $posibles = [];

                    var $posE = $ficha.posicion === 1 || $ficha.posicion === 4 ? $pos1 : $pos2;

                    if ($posE.x < $numFilas - 1 && $ficha.posicion !== 4){
                        $posibles.push({ x: $posE.x + 1, y: $posE.y });
                    }

                    if ($posE.y < $numColumnas - 1 && $ficha.posicion !== 1){
                        $posibles.push({ x: $posE.x, y: $posE.y + 1 });
                    }

                    if ($posE.x > 0 && $ficha.posicion !== 2){
                        $posibles.push({ x: $posE.x - 1, y: $posE.y });
                    }

                    if ($posE.y > 0 && $ficha.posicion !== 3){
                        $posibles.push({ x: $posE.x, y: $posE.y - 1 });
                    }

                    //console.log('Posibles valores extremo 1');
                    //console.log($posibles);

                    var $valido1 = false;
                    $.each($posibles, function(i, o) {
                        if (!_.isEmpty($celdas[o.x][o.y].ficha)){
                            var $extremo = $celdas[o.x][o.y].extremo;
                            var $ex = $ficha.extremo1;
                            
                            if ($extremo.respuesta === $ex.respuesta){
                                $valido1 = true;
                            }
                            else{
                                $valido1 = false;
                            }
                        }
                    });

                    $posibles = [];

                    $posE = $ficha.posicion === 2 || $ficha.posicion === 3 ? $pos1 : $pos2;

                    if ($posE.x < $numFilas - 1 && $ficha.posicion !== 2){
                        $posibles.push({ x: $posE.x + 1, y: $posE.y });
                    }

                    if ($posE.y < $numColumnas - 1 && $ficha.posicion !== 3){
                        $posibles.push({ x: $posE.x, y: $posE.y + 1 });
                    }

                    if ($posE.x > 0 && $ficha.posicion !== 4){
                        $posibles.push({ x: $posE.x - 1, y: $posE.y });
                    }

                    if ($posE.y > 0 && $ficha.posicion !== 1){
                        $posibles.push({ x: $posE.x, y: $posE.y - 1 });
                    }

                    //console.log('Posibles valores extremo 2');
                    //console.log($posibles);

                    var $valido2 = false;

                    $.each($posibles, function(i, o) {
                        if (!_.isEmpty($celdas[o.x][o.y].ficha)){
                            var $extremo = $celdas[o.x][o.y].extremo;
                            var $ex = $ficha.extremo2;

                            if ($extremo.respuesta === $ex.respuesta){
                                $valido1 = true;
                            }
                            else{
                                $valido1 = false;
                            }
                        }
                    });

                    $posicionar = $valido1 || $valido2;
                    if ($posicionar === false){
                        $(ui)[0].helper.css("top", 0);
                        $(ui)[0].helper.css("left", 0);
                        window.utils.reproducirSonido("mala_posicion");
                        //alert('La ficha no puede ponerse aqui');
                        $("#mensaje_mala_pos").css("display", "block");
                    }
                }

                if ($posicionar === true){

                    var $ex1 = {}; 
                    var $ex2 = {};

                    if ($ficha.posicion === 1 || $ficha.posicion === 4){
                        $ex1 = $ficha.extremo1;
                        $ex2 = $ficha.extremo2;
                    }
                    else{
                        $ex1 = $ficha.extremo2;
                        $ex2 = $ficha.extremo1;
                    }

                    $celdas[$pos1.x][$pos1.y] = { ficha: $ficha, extremo: $ex1 };
                    $celdas[$pos2.x][$pos2.y] = { ficha: $ficha, extremo: $ex2 };
                    
                    //console.log("Posiciones ocupadas: " + $pos1.x + "," + $pos1.y + " y " + $pos2.x + "," + $pos2.y);
                    
                    $(ui)[0].helper.removeClass('libre').addClass('fijo');
                    $(ui)[0].helper.draggable({ disabled: true });
                    
                    $historial.push({
                        ficha: $ficha.id,
                        pos1: $pos1,
                        pos2: $pos2
                    });

                    if ($(".bloque.libre").size() === 0){
                        
                        window.utils.reproducirSonido("victoria");
                        $("#inicio").load('./final.html', function(res) {
                            window.utils.establecerFondo("pantalla_gana");
                            $("a[rel='lnkRet']").click(function(e) {
                                e.preventDefault();
                                selectorOpController($, window, _);
                            });
                        });
                    }
                }
            }
            else{
                $(ui)[0].helper.css("top", 0);
                $(ui)[0].helper.css("left", 0);
                window.utils.reproducirSonido("mala_posicion");
                //alert('Ya hay una ficha en esta posicion');
                $("#mensaje_mala_pos").css("display", "block");
            }
        }
    });

    $("#mensaje_mala_pos").click(function() {
        $(this).css("display", "none");
    });
    
    $("#lnkInstrucciones").featherlight({ type: 'ajax' });
    
    /*$("#lnkInstrucciones").click(function(e) {
        e.preventDefault();
        //ayudaController($, window, _);
        $.featherlight();
    });*/
    
    $("#lnkReiniciar").click(function(e) {
        e.preventDefault();
        
        $("#inicio").load('./tablero.html', function(res) {
            tableroController($, window, _, $op);
        });
    });
    
    $("#lnkDeshacer").click(function(e) {
        e.preventDefault();
        
        if ($historial.length > 0){
            var $ultimo = _.last($historial);

            $celdas[$ultimo.pos1.x][$ultimo.pos1.y] = celda();
            $celdas[$ultimo.pos2.x][$ultimo.pos2.y] = celda();

            $("#" + $ultimo.ficha).css("top", 0);
            $("#" + $ultimo.ficha).css("left", 0);
            $("#" + $ultimo.ficha).draggable({ disabled: false });
            $("#" + $ultimo.ficha).removeClass("fijo").addClass("libre");

            _.pull($historial, $ultimo);            
        }
    });
    
    $('#contador').countdown({
        format: 'MS',
        until: +480,
        onExpiry: function(){
            $("#inicio").load('./final.html', function(res) {
                window.utils.reproducirSonido("pierde");
                window.utils.establecerFondo("pantalla_pierde");
                $("a[rel='lnkRet']").click(function(e) {
                    e.preventDefault();
                    selectorOpController($, window, _);
                });
            });
        },
        onTick: function(p){
            $('#texto_contador').text(_.padLeft(p[5], 2, '0') + ':' + _.padLeft(p[6], 2, '0')); 
        }
    });
    
    $("a#lnkRetTab").click(function(e) {
        e.preventDefault();
        selectorOpController($, window, _);
    });
};